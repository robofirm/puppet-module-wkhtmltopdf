class wkhtmltopdf (
  Enum['present', 'absent'] $ensure = 'present',
  String $version                   = '0.12.4',
) {

  if ('present' == $ensure) {
    # @todo Add error if attempting to install older version than 0.12.2 as it requires an X server and additional config
    # @todo Add consideration for different OS
    # @todo Add consideration for 32-bit
    $sourceFilename = "https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/${version}/wkhtmltox-${version}_linux-generic-amd64.tar.xz"

    exec { 'download_and_extract':
      command => "wget ${sourceFilename} -O wkhtmltox.tar.xz && tar xf wkhtmltox.tar.xz",
      cwd     => '/usr/local/src',
      creates => '/usr/local/src/wkhtmltopdf',
      require => [
        Package['wget'],
        Package['xz'],
      ],
    }

    file { '/usr/bin/wkhtmltoimage':
      ensure  => link,
      target  => '/usr/local/src/wkhtmltox/bin/wkhtmltoimage',
      mode    => '0755',
      require => Exec['download_and_extract'],
    }

    file { '/usr/bin/wkhtmltopdf':
      ensure  => link,
      target  => '/usr/local/src/wkhtmltox/bin/wkhtmltopdf',
      mode    => '0755',
      require => Exec['download_and_extract'],
    }

  } else {
    file { ['/usr/local/src/wkhtmltox', '/usr/bin/wkhtmltoimage', '/usr/bin/wkhtmltopdf']:
      ensure => absent,
    }
  }
}
